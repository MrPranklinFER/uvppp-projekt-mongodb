Prostorni referentni sustav
OGC Definition:
-----------------------------------
PROJCS["Austria Gauss-Krueger M34, Nord-5Mio.",
    GEOGCS["BESSEL_AUT",
        DATUM["BESSEL_AUT",
            SPHEROID["Bessel - 1841",6377397.155,299.1528128254262]],
        PRIMEM["Greenwich",0],
        UNIT["degree",0.0174532925199433]],
    PROJECTION["Transverse_Mercator"],
    PARAMETER["latitude_of_origin",0],
    PARAMETER["central_meridian",16.33333333],
    PARAMETER["scale_factor",1],
    PARAMETER["false_easting",0],
    PARAMETER["false_northing",-5000000],
    UNIT["METER",1]]

===============================================================================

Nakon importiranja, poželjno je napraviti slijedeće korekcije/ažuriranja:

Tablica/Feature: KoristenjeZemljista
Vrijednosti atributa TIP promijeniti u hrvatske:
Abbaufläche                          Nekultivirana površina
Bahnanlage                           Željezničko postrojenje
Baufläche befestigt                  Građevinska površina
Baufläche begrünt                    Građevinska zelena površina
Brachland                            Ugar
Erholungsfläche                      Odmor i rekreacija
Garten                               Vrt
Gebäude                              Zgrada
Gewässer fließend                    Voda tekuća
Gewässer stehend                     Voda stajaća
Hutweide                             Pašnjak
Lagerplatz                           Skladište
Landw. genutzt                       Poljoprivredno
Ödland                               Pustopoljina
Sonstige                             Ostala namjena
Straßenanlage                        Ulično postrojenje
Streuobstwiese                       Voćnjak
Streuwiese                           Močvarna livada
Techn. Ver/Entsorgungsanlage         Infrastrukturni uređaj
Wald                                 Šuma
Weingarten                           Vinograd
Werksgelände                         Tvornička zona
Wiese                                Livada


Tablica/Feature: MedjnaTocka
Vrijednosti atributa TIP promijeniti u hrvatske:

GP gekennzeichnet                    Označena
GP gekennzeichnet keine Punktnummer  Označena - bez broja
GP nicht gekennzeichnet              Neoznačena


Tablica/Feature: MedjnaTocka
Vrijednosti atributa TIP promijeniti u hrvatske:

EP                       Priključna točka
HP                       Reper
TP-Bodenpunkt            Triangulacija - točka na zemlji
TP-Kirche                Triangulacija - crkva
TP-Sonstiger Hochpunkt   Triangulacija - visoka točka

==================================================================================================================

Q1: Kat. čestice u kojima se nalazi geodetska točka

Q2: Najveća kat. česticu u graničnom katastru (vrijednost atributa GranicniKatastar = 'G') i sve njene međne točke

Q3: Kat. čestice na kojima je u cijelosti vinograd

Q4: Najveću vinogradarsku kat. česticu i sve njene susjede čestice

Q5. Kat. čestice koje se u cijelosti koriste kao 'Poljoprivredno' zemljište

Q6: Kat. čestice koje su u cijelosti "pokrivene" zgradom

Q7: Kat. čestice koje se u cijelosti nalaze na planu '7532G291'

==================================================================================================================

Hive Q: Broj geodetskih točaka u svakoj katastarskoj općini (relevantne su samo točke koje se nalaye unutar kat. općine)  
